package com.hrynyk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * class with main work.
 */
public class Fibonacci {
    /**
     * var which help to build Fibonacci Series.
     */
    private int b = 0;
    /**
     * var which help to build Fibonacci Series.
     */
    private int c = 1;
    /**
     * var which need to save max odd number.
     */
    private int odd = 0;
    /**
     * var which need to save max even number.
     */
    private int even = 0;
    /**
     * var which help me to work with indexes in list.
     */
    private int index;
    /**
     * initialize scanner to input number.
     */
    private Scanner input = new Scanner(System.in, "UTF-8");
    /**
     * initialize list.
     */
    private List<Integer> fibonacciList = new ArrayList<Integer>();

    /**
     * method to build com.hrynyk.Fibonacci series.
     */
    final void buildFibonacciSeries() {
        System.out.println("Enter limit of Series: ");
        int limitNumbers = input.nextInt();
        for (int i = 0; i <= limitNumbers; i++) {
            int numberFibonacci = b;
            b = c;
            c = numberFibonacci + b;
            fibonacciList.add(numberFibonacci);
        }
    }

    /**
     * method to find max odd number.
     */
    final void findMaxOddNumber() {
        int max = Collections.max(fibonacciList);
        for (int k = 0; k < fibonacciList.size(); k++) {
            if (!(max % 2 == 1)) {
                index = fibonacciList.indexOf(max);
                max = fibonacciList.get(index - 1);
            } else {
                odd = max;
            }
        }

        System.out.println("Max odd number is: " + odd);
    }

    /**
     * method to find maz even number.
     */
    final void findMaxEvenNumber() {
        int max = Collections.max(fibonacciList);
        for (int j = 0; j < fibonacciList.size(); j++) {
            if (!(max % 2 == 0)) {
                index = fibonacciList.indexOf(max);
                max = fibonacciList.get(index - 1);
            } else {
                even = max;
            }
        }
        System.out.println("Max even number is: " + even);
    }

    /**
     * method to find percentages of odd and even in series.
     */
    final void findPercentages() {
        int evenCounter = 0;
        int oddCounter = 0;
        float oddPercentages;
        float evenPercentages;
        for (int n = 1; n < fibonacciList.size(); n++) {
            if (fibonacciList.get(n) % 2 == 0) {
                evenCounter = evenCounter + 1;
                System.out.println("Even counter" + evenCounter);
            } else {
                oddCounter = oddCounter + 1;
            }
        }

        /**
          Find Percentages
         */
        final float percentageNumber = 100f;
        oddPercentages = (oddCounter * percentageNumber)
                / (fibonacciList.size());
        evenPercentages = (evenCounter * percentageNumber)
                / (fibonacciList.size());

        System.out.println("Percentages of odd numbers: "
                + oddPercentages + "%");
        System.out.println("Percentages of even numbers: "
                + evenPercentages + "%");
    }
}
