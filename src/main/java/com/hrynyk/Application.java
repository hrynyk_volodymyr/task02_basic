package com.hrynyk;

/**
 * First project on java.
 * @author Hrynyk Volodymyr
 */
public class Application {
    /**
     *@param args Fibonacci.class
     */
    public static void main(String[] args) {
        Fibonacci fibonacci = new Fibonacci();

        fibonacci.buildFibonacciSeries();
        fibonacci.findMaxOddNumber();
        fibonacci.findMaxEvenNumber();
        fibonacci.findPercentages();
    }
}
